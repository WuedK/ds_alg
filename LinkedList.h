#pragma once

# ifndef LINKED_LIST_H
# define LINKED_LIST_H

# include <limits.h>
# include <iostream>

namespace dsalg {

	int abs(int a, int b) {
		return ((a - b) >= 0) ? (a - b) : (b - a);
	}


	template<typename T>
	class LinkedListElement {
	private:
		T value;
		LinkedListElement<T>* next;
		LinkedListElement<T>* previous;
	public:
		T& getValue() { return value; }
		void setValue(T value) { this->value = value; }
		void setNext(LinkedListElement<T>* next) { this->next = next; }
		LinkedListElement<T>* peekNext() { return next; }
		void setPrevious(LinkedListElement<T>* previous) { this->previous = previous; }
		LinkedListElement<T>* peekPrevious() { return previous; }
		

		LinkedListElement<T>(T value, LinkedListElement<T>* previous, LinkedListElement<T>* next) {
			this->value = value;
			this->previous = previous;
			this->next = next;
		}

	};


	template<typename T>
	class LinkedList {
	private:

		int size;
		LinkedListElement<T>* first;
		LinkedListElement<T>* last;
		LinkedListElement<T>* pos;
		int posIndex;
		
	public:
		class iterator;

	public:

		int getPosition() {
			return posIndex;
		}

		int length() {
			return size;
		}


		bool changePositon(int index) {
			if ((index < 0) or (index >= size))
				return false;
			else if (index == posIndex)
				return true;
			else if (index == 0) {
				pos = first;
				posIndex = 0;
			}
			else if (index == size - 1) {
				pos = last;
				posIndex = size - 1;
			}
			else {
				index -= posIndex;
				if (index < 0)
					goBack(0 - index);
				else
					goForward(index);
			}
			return true;
		}

		bool goBack(int times = 1){
			if ((size <= 1) or (times <= 0) or (posIndex - times < 0))
				return false;
			int tempPosIndex = posIndex - times;
			if (tempPosIndex < posIndex - tempPosIndex) {
				pos = first;
				posIndex = 0;
				for (;posIndex < tempPosIndex; posIndex++)
					pos = pos->peekNext();
			}
			else {
				for (;posIndex > tempPosIndex; posIndex--)
					pos = pos->peekPrevious();
			}
			return true;
			
		}

		bool goForward(int times = 1) {
			if ((size <= 1) or (times <= 0) or (posIndex + times >= size))
				return false;
			int tempPosIndex = posIndex + times;
			if (tempPosIndex - posIndex < size - 1 - tempPosIndex) {			
				for (;posIndex < tempPosIndex; posIndex++)
					pos = pos->peekNext();
			}
			else {
				pos = last;
				posIndex = size - 1;
				for (;posIndex > tempPosIndex; posIndex--)
					pos = pos->peekPrevious();
			}
			return true;
		}


		bool goBackTillSee(T val) {
			if (size == 0)
				return false;
			if (pos->getValue() == val)
				return true;
			LinkedListElement<T>* tempPos = pos;
			int tempPosIndex = posIndex;
			while (tempPos->peekPrevious() != nullptr) {
				tempPos = tempPos->peekPrevious();
				tempPosIndex--;
				if (tempPos->getValue() == val) {
					pos = tempPos;
					posIndex = tempPosIndex;
					return true;
				}
			}
			return false;
		}

		bool goForwardTillSee(T val) {
			if (size == 0)
				return false;
			if (pos->getValue() == val)
				return true;
			LinkedListElement<T>* tempPos = pos;
			int tempPosIndex = posIndex;
			while (tempPos->peekNext() != nullptr) {
				tempPos = tempPos->peekNext();
				tempPosIndex++;
				if (tempPos->getValue() == val) {
					pos = tempPos;
					posIndex = tempPosIndex;
					return true;
				}
			}
			return false;
		}

		bool insert(T val, int index = posIndex) {
			if ((index > size) or (index < 0) or (size == INT_MAX - 1))
				return false;
			else {
				if (size == 0) {
					first = new LinkedListElement<T>(val, nullptr, nullptr);
					last = first;
					pos = first;
					posIndex = 0;
				}
				else if (index == 0) {
					first = new LinkedListElement<T>(val, nullptr, first);
					first->peekNext()->setPrevious(first);
					if (posIndex == index)
						pos = first;
					else
						posIndex++;
				}
				else if (index == size) {
					last = new LinkedListElement<T>(val, last, nullptr);
					last->peekPrevious()->setNext(last);
				}
				else if (posIndex == index) {
					pos = new LinkedListElement<T>(val, pos->peekPrevious(), pos);
					pos->peekNext()->setPrevious(pos);
				}
				else {
					LinkedListElement<T>* tempPos = pos;
					int tempPosIndex = (index < posIndex) ? posIndex++ : posIndex;
					changePositon(index);
					pos = new LinkedListElement<T>(val, pos->peekPrevious(), pos);
					pos->peekNext()->setPrevious(pos);
					pos = tempPos;
					posIndex = tempPosIndex;
				}
				size++;
				return true;
			}
			
		}


		bool remove(int index = posIndex) {
			if ((index < 0) or (index >= size) or (size == 0))
				return false;
			else if (size == 1) {
				delete pos;
				pos = nullptr;
				first = nullptr;
				last = nullptr;
				posIndex = -1;
			}
			else if (index == 0) {
				LinkedListElement<T>* tempPos = first;
				first = first->peekNext();
				first->setPrevious(nullptr);
				delete tempPos;
				if (index == posIndex)
					pos = first;
			}
			else if (index == size - 1) {
				LinkedListElement<T>* tempPos = last;
				last = last->peekPrevious();
				last->setNext(nullptr);
				delete tempPos;
				if (index == posIndex) {
					pos = last;
					posIndex = size - 2;
				}
			}
			else {
				LinkedListElement<T>* tempPos = pos;
				if (index == posIndex) {				
					pos->peekNext()->setPrevious(pos->peekPrevious());
					pos->peekPrevious()->setNext(pos->peekNext());
					pos = pos->peekNext();
					delete tempPos;
				}
				else {
					int tempPosIndex = (index < posIndex) ? posIndex-- : posIndex;
					changePositon(index);
					pos->peekNext()->setPrevious(pos->peekPrevious());
					pos->peekPrevious()->setNext(pos->peekNext());
					delete pos;
					pos = tempPos;
					posIndex = tempPosIndex;
				}

				size--;
				return true;
			}
		}

		T getValue(int index = posIndex) {
			if ((index < 0) or (index >= size)) {
				return NULL;
			}
			if (index == posIndex) {
				T val = pos->getValue();
				return val;
			} 
			else {
				LinkedListElement<T>* tempPos = pos;
				int tempPosIndex = posIndex;
				changePositon(index);
				T val = pos->getValue();
				pos = tempPos;
				posIndex = tempPosIndex;
				return val;
			}
		}


		T& getValueRefrence(int index = posIndex) {
			if ((index < 0) or (index >= size)) {
				return NULL;
			}
			if (index == posIndex) {
				return pos->getValue();
			}
			else {
				LinkedListElement<T>* tempPos = pos;
				int tempPosIndex = posIndex;
				changePositon(index);
				T val = pos->getValue();
				pos = tempPos;
				posIndex = tempPosIndex;
				return &val;
			}
		}

		bool setValue(T val, int index = posIndex) {
			if ((index < 0) or (index >= size)) {
				return false;
			}
			if (index == posIndex) 
				pos->setValue(val);
			else {
				LinkedListElement<T>* tempPos = pos;
				int tempPosIndex = posIndex;
				changePositon(index);
				pos->setValue(val)
				pos = tempPos;
				posIndex = tempPosIndex;
			}
			return true;
		}

		void sort() {

		}



		iterator begin() {
			return iterator(first);
		}

		iterator end() {
			return iterator(last);
		}

		LinkedList() {
			size = 0;
			first = nullptr;
			last = nullptr;
			pos = nullptr;
			posIndex = -1;
		}

		~LinkedList() {
			pos = first;
			first = nullptr;
			last = nullptr;
			posIndex = -1;
			while (size--) {
				pos = pos->peekNext();
				pos->peekPrevious()->setPrevious(nullptr);
				pos->peekPrevious()->setNext(nullptr);
				delete pos->peekPrevious();
			}
		}
	};


	template<typename T>
	class LinkedList<T>::iterator {
	private:
		LinkedListElement* pos;
	public:
		iterator(LinkedListElement* s_pos) : pos(s_pos) {

		}

		iterator& operator++() {
			pos->goForward();

			return *this;
		}

		iterator& operator++(int) {
			pos->goForward();

			return *this;
		}

		T& operator*() {
			return pos->getValue();
		}

		bool operator!=(const iterator& other) const {
			return pos != other.pos;
		}

		bool operator==(const iterator& other) const {
			return pos == other.pos;
		}
	};


	

}


# endif
