#pragma once

#ifndef SORTS_H
#define SORTS_H

namespace dsalg {

	template <typename T>
	void swap(T&, T&);

	template <typename T>
	void bubbleSort(T[], int, bool sortInIncesingOrder = true);


	template <typename T>
	void insertionSort(T[], int, bool sortInIncesingOrder = true);




	template <typename T>
	void swap(T& e1, T& e2) {
		T temp = e1;
		e1 = e2;
		e2 = temp;
	}

	template <typename T>
	void bubbleSort(T arr[], int size, bool sortInIncesingOrder) {
		if (!sortInIncesingOrder) {
			for (int i = 0; i < size - 1; i++) {
				bool isSorted = true;
				for (int j = 0; j < size - i - 1; j++) {
					if (arr[j] < arr[j + 1]) {
						swap(arr[j], arr[j + 1]);
						isSorted = false;
					}
				}
				if (isSorted)
					break;
			}
		}
		else {
			for (int i = 0; i < size - 1; i++) {
				bool isSorted = true;
				for (int j = 0; j < size - i - 1; j++) {
					if (arr[j] > arr[j + 1]) {
						swap(arr[j], arr[j + 1]);
						isSorted = false;
					}
				}
				if (isSorted)
					break;
			}
		}
	}


	template <typename T>
	void insertionSort(T arr[], int size, bool sortInIncesingOrder) {
		if (sortInIncesingOrder) {
			for (int i = 1; i < size; i++) {
				T key = arr[i];
				int j = i;
				while ((j > 0) && (key < arr[j - 1])) {
					arr[j] = arr[j - 1];
					j--;
				}
				arr[j] = key;
			}
		}
		else {
			for (int i = 1; i < size; i++) {
				T key = arr[i];
				int j = i;
				while ((j > 0) && (key > arr[j - 1])) {
					arr[j] = arr[j - 1];
					j--;
				}
				arr[j] = key;
			}
		}
	}

}



#endif